from gensim.models import KeyedVectors

ads = [
    [ "Razer Deathadder Gaming Mouse", ["computer","videogame"], ["razer","deathadder","gaming","mouse","pc","mac","steam","fps","moba","dpi"] ],
    [ "New ASOS Summer Range", ["clothing","clothes","range","shorts","skirt","bikini","dress","polo shirt","swimwear","hat"], ["asos","womens","mens","t-shirt","crop top","baseball cap","bucket hat","collection"] ],
    [ "Buy Bitcoin on Binance", ["invest","buy","trade","trading","money"], ["bitcoin","crypto","cryptocurrency","binance","ethereum","coinbase","cardano","btc","eth","bnb","avax","ada","kucoin","coingecko"] ],
    [ "ASM Hydrasynth Explorer", ["music","producer","musician","artist","studio","keyboard"], ["digital","synthesiser","polyphonic","analog","analogue","gear4music","pmt","andertons"] ],
    [ "Trade stocks on InteractiveBrokers", ["invest","buy","trade","trading","stocks","commodities","money"], ["options","futures","derivatives","stock market","forex","gold","oil","silver"] ],
    [ "Rift VST", ["music","producer","musician","artist","studio"], ["distortion","production","serum","ableton","fl studio","logic","thermal","trash","software"] ],
    [ "Carbon Fibre Bicycle Fork", ["bike","bicycle"], ["mtb","mountain bike","suspension",'26"',"bikester","26inch","26 inch"] ],
    [ "GTA 6", ["console","videogame","computer"], ["xbox","playstation","pc","grand theft auto","rockstar"] ],
    [ "Cheap train tickets to Sheffield", ["cheap","train","tickets"], ["sheffield","trainline","coach","return","one-way","one way","single","station"] ],
    [ "TASCAM Cassette Recorder", ["music","producer","musician","artist","studio","hardware","sampler","sampling","cassette","tape","tape deck","recording","recorder"], ["ableton","fl studio","logic","bitwig"] ],
    [ "50% off Javascript Courses", ["programmer","coding","course","discount","developer"], ["javscript"] ] ]

nlp_model = KeyedVectors.load_word2vec_format('model2.bin', binary=False)
keywords = [[] for i in ads]

for num, ad in enumerate(ads):
    for tag in ad[1]:
        keywords[num].append(tag)
        try:
            n = 0
            for word in nlp_model.most_similar(positive=[tag], topn=8):
                if word[0].lower().replace("_", " ") not in keywords[num]:
                    keywords[num].append(word[0].lower().replace("_", " "))
                    n += 1
                if n == 5:
                    break
        except KeyError as e:
            print(e)
            continue

for n, ad in enumerate(keywords):
    print("\n\n" + ads[n][0])
    print("User-specified tags:")
    for word in ads[n][2]:
        print(word, end=', ')
    for word in ads[n][1]:
        print(word, end=', ')
    print("\nModel-generated tags:")
    i = 0
    for tag in ad:
        while i < len(ads[n][1]):
            i += 1
            continue
        print(tag, end=', ')